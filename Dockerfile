FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app

COPY ModernMilkmanSEIT/*.csproj ./ModernMilkmanSEIT/
WORKDIR /app/ModernMilkmanSEIT
RUN dotnet restore

FROM build as modernMilkmanTests
WORKDIR /app/ModernMilkmanSEIT
RUN dotnet test --no-restore --configuration Docker --output out
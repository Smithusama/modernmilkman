﻿using System;
using FluentAssertions;
using FluentAssertions.Execution;
using ModernMilkmanSEIT.Pages;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace ModernMilkmanSEIT.Steps
{
    [Binding]
    public class UserLoginSteps
    {
        private readonly LoginPage _loginPage;
        private readonly HomePage _homePage;
        public UserLoginSteps(IWebDriver webDriver)
        {
            _loginPage = new LoginPage(webDriver);
            _homePage = new HomePage(webDriver);
        }

        [Given(@"the user has navigated to the login page")]
        public void GivenTheUserHasNavigatedToTheLoginPage()
        {
            _loginPage.NavigateTo();
        }
        
        [Given(@"the user has entered their (.*) and (.*)")]
        public void GivenTheUserHasEnteredTheirMobileNumberAndPassword(string mobileNumber, string password)
        {
            _loginPage.InputMobileNumber(mobileNumber);
            _loginPage.InputPassword(password);
        }
        
        [When(@"the user presses the login button")]
        public void WhenTheUserPressesTheLoginButton()
        {
            _loginPage.SubmitLoginButton();
        }
        
        [Then(@"the user is logged in and navigated to the home page")]
        public void ThenTheUserIsLoggedInAndNavigatedToTheHomePage()
        {
            _homePage.IsUserLoggedIn().Should().BeTrue();
        }

        [Then(@"the user is shown the incorrect login details modal")]
        public void ThenTheUserIsShownTheIncorrectLoginDetailsModal()
        {
            _loginPage.IsIncorrectLoginModalDisplayed().Should().BeTrue();
        }

        [Then(@"the incorrect login details message is displayed")]
        public void ThenTheIncorrectLoginDetailsMessageIsDisplayed()
        {
            _loginPage.GetIncorrectLoginModalText().Should().BeEquivalentTo("Your phone number or password is incorrect. Please try again.");
        }

        [Then(@"the user is shown the error phone number validation message")]
        public void ThenTheUserIsShownTheErrorPhoneNumberValidationMessage()
        {
            using (new AssertionScope())
            {
                _loginPage.IsErrorPhoneNumberValidationMessageDisplayed().Should().BeTrue();
                _loginPage.GetErrorPhoneNumberValidationMessageText().Should().BeEquivalentTo("Please enter your phone number");
            }
        }

        [Then(@"the user is shown the error password validation message")]
        public void ThenTheUserIsShownTheErrorPasswordValidationMessage()
        {
            using (new AssertionScope())
            {
                _loginPage.IsErrorPasswordValidationMessageDisplayed().Should().BeTrue();
                _loginPage.GetErrorPasswordValidationMessageText().Should().BeEquivalentTo("Please provide a password");
            }
        }

    }
}

﻿using System;
using FluentAssertions;
using ModernMilkmanSEIT.Pages;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace ModernMilkmanSEIT.Steps
{
    [Binding]
    public class UserAccountDetailsSteps
    {
        private readonly LoginPage _loginPage;
        private readonly HomePage _homePage;
        private readonly MyAccountPage _myAccountPage;
        private readonly AccountDetailsPage _accountDetailsPage;

        public UserAccountDetailsSteps(IWebDriver webDriver)
        {
            _loginPage = new LoginPage(webDriver);
            _homePage = new HomePage(webDriver);
            _myAccountPage = new MyAccountPage(webDriver);
            _accountDetailsPage = new AccountDetailsPage(webDriver);
        }

        [Given(@"the user is logged in")]
        public void GivenTheUserIsLoggedIn()
        {
            _loginPage.NavigateTo();
            _loginPage.InputMobileNumber("07522216593");
            _loginPage.InputPassword("Film8TofuCircle");
            _loginPage.SubmitLoginButton();
            _homePage.IsUserLoggedIn();
        }
        
        [Given(@"the user has navigated to the account details page")]
        public void GivenTheUserHasNavigatedToTheAccountDetailsPage()
        {
            _accountDetailsPage.NavigateTo();
            _accountDetailsPage.IsTheAccountDetailsPageOpen();
        }
        
        [When(@"the user opens the account dropdown")]
        public void WhenTheUserOpensTheAccountDropdown()
        {
            _homePage.ExpandAccountDropdown();
        }
        
        [When(@"the user selects my account from account dropdown")]
        public void WhenTheUserSelectsMyAccountFromAccountDropdown()
        {
            _homePage.ClickMyAccount();
            _myAccountPage.IsMyAccountPageOpen();
        }
        
        [When(@"the user clicks the account details button")]
        public void WhenTheUserClicksTheAccountDetailsButton()
        {
            _myAccountPage.ClickAccountDetails();
            _myAccountPage.IsMyAccountPageOpen();
        }
        
        [When(@"the user edits their (.*)")]
        public void WhenTheUserEditsTheirEmail(string email)
        {
            _accountDetailsPage.InputEmail(email);
        }
        
        [When(@"the user clicks the save button")]
        public void WhenTheUserClicksTheSaveButton()
        {
            _accountDetailsPage.SubmitSaveButton();
        }
        
        [Then(@"the user is on the account details page")]
        public void ThenTheUserIsOnTheAccountDetailsPage()
        {
            _accountDetailsPage.IsTheAccountDetailsPageOpen().Should().BeTrue();
        }
        
        [Then(@"the successfully updated account modal is displayed")]
        public void ThenTheSuccessfullyUpdatedAccountModalIsDisplayed()
        {
            _accountDetailsPage.IsAccountUpdateSuccessModalDisplayed().Should().BeTrue();
        }
        
        [Then(@"the invalid email validation message is displayed")]
        public void ThenThePleaseInvalidEmailValidationMessageIsDisplayed()
        {
            _accountDetailsPage.IsInvalidEmailValidationMessageDisplayed().Should().BeTrue();
        }
    }
}

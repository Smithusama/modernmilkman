﻿using System;
using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow;
using TechTalk.SpecRun;

namespace ModernMilkmanSEIT.Hooks.ScenarioRun
{
    [Binding]
    public class WebDriverSetupHook
    {
        [BeforeScenario]
        public static void BeforeScenario(IObjectContainer objectContainer, TestRunContext testRunContext)
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            //chromeOptions.AddArgument("headless");
            chromeOptions.AddArgument("start-maximized");

            IWebDriver webDriver = new ChromeDriver(ChromeDriverService.CreateDefaultService(testRunContext.TestDirectory), chromeOptions);
            //IWebDriver webDriver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), chromeOptions);
            objectContainer.RegisterInstanceAs(webDriver);
        }

        [AfterScenario]
        public static void AfterScenario(IWebDriver webDriver)
        {
            webDriver.Quit();
        }
    }
}

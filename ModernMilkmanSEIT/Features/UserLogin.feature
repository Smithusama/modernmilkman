﻿Feature: UserLogin
	Website login page responsible for authentication and redirecting users

Scenario: User is able to log in with valid details
	Given the user has navigated to the login page
	And the user has entered their <mobile number> and <password>
	When the user presses the login button
	Then the user is logged in and navigated to the home page
	Examples: 
	| mobile number | password        |
	| 07522216593   | Film8TofuCircle |

##You could use the above to do a negative test too in theory, passing in something like "isCorrectLoginDetails"
##I feel like explicitly having a test scenario tht checks invalid details can't log in is more readable in the code and in reports produced
##Plus you don't need branching logic this way, so can check the modal appears

Scenario: User is unable to log in with invalid details
	Given the user has navigated to the login page
	And the user has entered their <mobile number> and <password>
	When the user presses the login button
	Then the user is shown the incorrect login details modal
	Then the incorrect login details message is displayed
	Examples: 
	| mobile number | password       |
	| 07777777777   | notThePassword |

Scenario: User is shown validation messages if they do not enter any details
	Given the user has navigated to the login page
	And the user has entered their <mobile number> and <password>
	When the user presses the login button
	Then the user is shown the error phone number validation message
	Then the user is shown the error password validation message

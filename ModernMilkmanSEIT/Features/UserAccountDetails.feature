﻿Feature: UserAccountDetails
	Website login page responsible for displaying and updating user account details

Scenario: User is able to manually navigate to the account details page
	Given the user is logged in
	When the user opens the account dropdown
	And the user selects my account from account dropdown
	And the user clicks the account details button
	Then the user is on the account details page

Scenario: User is able to update their email with a valid email address
	Given the user is logged in
	And the user has navigated to the account details page
	When the user edits their <email address>
	And the user clicks the save button
	Then the successfully updated account modal is displayed
	Examples: 
	| email address             |
	| jakeleaning@hotmail.co.uk |

##Similar to account login, the above test could include negative testing, but seperating them removes need for branching checks and reporting is easier to read

Scenario: User is unable to update their email with an invalid email address
	Given the user is logged in
	And the user has navigated to the account details page
	When the user edits their <email address>
	And the user clicks the save button
	Then the invalid email validation message is displayed
	Examples: 
	| email address              |
	| jakeleaning.co.uk          |
	| jakeleaning@@hotmail.co.uk |
	| jakeleaning@jakeleaning    |
	

	
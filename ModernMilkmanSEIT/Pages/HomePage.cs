﻿using ModernMilkmanSEIT.Drivers;
using OpenQA.Selenium;

namespace ModernMilkmanSEIT.Pages
{
    public class HomePage : Page
    {
        private readonly IWebDriver _webDriver;
        public HomePage(IWebDriver webDriver) : base(webDriver)
        {
            _webDriver = webDriver;
        }

        private const string PageUrl = "https://cust.themodernmilkman.co.uk/";
        private readonly By _homeBannerBy = By.ClassName("banner-homepage");
        private readonly By _nextDeliveryBy = By.ClassName("next-del1");
        private readonly By _accountDropdownBy = By.ClassName("account");
        private readonly By _myAccountLinkBy = By.LinkText("My account");

        public void NavigateTo()
        {
            base.NavigateTo(PageUrl, _homeBannerBy);
        }

        public bool IsUserLoggedIn()
        {
            return _webDriver.ElementExists(_nextDeliveryBy);
        }

        public void ExpandAccountDropdown()
        {
            _webDriver.WaitAndFindElement(_accountDropdownBy).Click();
        }

        public void ClickMyAccount()
        {
            _webDriver.WaitAndFindElement(_myAccountLinkBy).Click();
        }
    }
}

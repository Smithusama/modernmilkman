﻿using ModernMilkmanSEIT.Drivers;
using OpenQA.Selenium;

namespace ModernMilkmanSEIT.Pages
{
    public class Page
    {
        private readonly IWebDriver _webDriver;

        public Page(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void NavigateTo(string url, By pageIsNavigatedToElementToCheck, int timeout = 10)
        {
            _webDriver.Navigate().GoToUrl(url);

            _webDriver.WaitForElement(pageIsNavigatedToElementToCheck, timeout);
        }
    }
}

﻿using ModernMilkmanSEIT.Drivers;
using OpenQA.Selenium;

namespace ModernMilkmanSEIT.Pages
{
    public class LoginPage : Page
    {
        private readonly IWebDriver _webDriver;
        public LoginPage(IWebDriver webDriver) : base(webDriver)
        {
            _webDriver = webDriver;
        }

        private const string PageUrl = "https://cust.themodernmilkman.co.uk/Users/login";
        private readonly By _loginButtonBy = By.Id("checkLogin");
        private readonly By _mobileNumberInputBy = By.Id("phoneNo");
        private readonly By _passwordInputBy = By.Id("password");
        private readonly By _incorrectLoginModalBy = By.Id("swal2-content");
        private readonly By _phoneNumberErrorValidationMessageBy = By.Id("phoneNo-error");
        private readonly By _passwordErrorValidationMessageBy = By.Id("password-error");

        public void NavigateTo()
        {
            base.NavigateTo(PageUrl, _loginButtonBy);
        }

        #region Inputs
        public void InputMobileNumber(string mobileNumber)
        {
            _webDriver.WaitAndFindElement(_mobileNumberInputBy).SendKeys(mobileNumber);
        }

        public void InputPassword(string password)
        {
            _webDriver.WaitAndFindElement(_passwordInputBy).SendKeys(password);
        }
        public void SubmitLoginButton()
        {
            _webDriver.WaitAndFindElement(_loginButtonBy).Click();
        }
        #endregion

        #region Gets
        public string GetIncorrectLoginModalText()
        {
            return _webDriver.WaitAndFindElement(_incorrectLoginModalBy).Text;
        }
        public string GetErrorPhoneNumberValidationMessageText()
        {
            return _webDriver.WaitAndFindElement(_phoneNumberErrorValidationMessageBy).Text;
        }
        public string GetErrorPasswordValidationMessageText()
        {
            return _webDriver.WaitAndFindElement(_passwordErrorValidationMessageBy).Text;
        }
        #endregion

        #region IsDisplayed
        public bool IsIncorrectLoginModalDisplayed()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_incorrectLoginModalBy);
        }

        public bool IsErrorPhoneNumberValidationMessageDisplayed()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_phoneNumberErrorValidationMessageBy);
        }

        public bool IsErrorPasswordValidationMessageDisplayed()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_passwordErrorValidationMessageBy);
        }
        #endregion



    }
}

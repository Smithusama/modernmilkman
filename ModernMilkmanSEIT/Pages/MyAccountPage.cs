﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModernMilkmanSEIT.Drivers;
using OpenQA.Selenium;

namespace ModernMilkmanSEIT.Pages
{
    public class MyAccountPage : Page
    {
        private readonly IWebDriver _webDriver;
        public MyAccountPage(IWebDriver webDriver) : base(webDriver)
        {
            _webDriver = webDriver;
        }

        private const string PageUrl = "https://cust.themodernmilkman.co.uk/users/myAccount";
        private readonly By _myAccountSectionBy = By.ClassName("myaccount-section");
        private readonly By _accountDetailsLinkBy = By.PartialLinkText("Account details");

        public void NavigateTo()
        {
            base.NavigateTo(PageUrl, _myAccountSectionBy);
        }

        public void ClickAccountDetails()
        {
            _webDriver.WaitAndFindElement(_accountDetailsLinkBy).Click();
        }

        public void IsMyAccountPageOpen()
        {
            _webDriver.ElementExistsAndIsDisplayed(_myAccountSectionBy);
        }
    }
}

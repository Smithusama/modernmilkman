﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModernMilkmanSEIT.Drivers;
using OpenQA.Selenium;

namespace ModernMilkmanSEIT.Pages
{
    public class AccountDetailsPage : Page
    {
        private readonly IWebDriver _webDriver;
        public AccountDetailsPage(IWebDriver webDriver) : base(webDriver)
        {
            _webDriver = webDriver;
        }

        private const string PageUrl = "https://cust.themodernmilkman.co.uk/users/account-details";
        private readonly By _accountInformationBy = By.ClassName("account-information");
        private readonly By _emailInputBy = By.Id("email");
        private readonly By _saveButtonBy = By.Id("update_profile");
        private readonly By _accoundUpdateSuccessModalBy = By.ClassName("swal2-icon-success");
        private readonly By _invalidEmailValidationMessageBy = By.Id("error-otp");

        public void NavigateTo()
        {
            base.NavigateTo(PageUrl, _accountInformationBy);
        }

        public void InputEmail(string email)
        {
            IWebElement emailBox = _webDriver.WaitAndFindElement(_emailInputBy);
            emailBox.Clear();
            emailBox.SendKeys(email);
        }

        public void SubmitSaveButton()
        {
            _webDriver.WaitAndFindElement(_saveButtonBy).Click();
        }

        public bool IsTheAccountDetailsPageOpen()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_accountInformationBy);
        }

        public bool IsAccountUpdateSuccessModalDisplayed()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_accoundUpdateSuccessModalBy);
        }

        public bool IsInvalidEmailValidationMessageDisplayed()
        {
            return _webDriver.ElementExistsAndIsDisplayed(_invalidEmailValidationMessageBy);
        }

    }
}

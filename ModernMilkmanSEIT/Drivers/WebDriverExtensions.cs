﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ModernMilkmanSEIT.Drivers
{
    public static class WebDriverExtensions
    {
        public static IWebElement WaitAndFindElement(this IWebDriver webDriver, By by, int timeout = 10)
        {
            webDriver.WaitForElement(by, timeout);
            return webDriver.FindElement(by);
        }

        public static bool ElementExists(this IWebDriver webDriver, By by, int timeout = 10)
        {
            try
            {
                webDriver.WaitForElement(by, timeout);
                return true;
            }
            catch(NoSuchElementException)
            {
                return false;
            }
        }

        public static bool ElementExistsAndIsDisplayed(this IWebDriver webDriver, By by, int timeout = 10)
        {
            return webDriver.ElementExists(by, timeout) && webDriver.WaitAndFindElement(by, timeout).Displayed;
        }

        public static void WaitForElement(this IWebDriver webDriver, By elementBy, int timeout = 10)
        {
            WebDriverWait webDriverWait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout));
            webDriverWait.IgnoreExceptionTypes(typeof(NoSuchElementException), typeof(ElementNotVisibleException));

            webDriverWait.Until(driver =>
            {
                IWebElement webElement = driver.FindElement(elementBy);
                if (webElement != null && webElement.Displayed && webElement.Enabled)
                {
                    return webElement;
                }

                return null;
            });
        }
    }
}

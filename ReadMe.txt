Got a bit stuck on the docker part:

I can easily run a headless chrome browser in docker and run my tests against it with:
docker run -d -p 4444:4444 selenium/standalone-chrome:3.141.59-yttrium
dotnet test

Which could be expanded with selenium-hub to setup different environments

But that is more just "running tests against a dockerised Chrome instance"
Where as I wanted the tests themselves ran IN a container.

I ran out of time while researching but essentially something like:
	-Docker compose file
		-One service which is the web browser
		-A second service which is the tests themselves
	-Dockerfile that pulls .NET:5.0, restores, builds and calls dotnet test

